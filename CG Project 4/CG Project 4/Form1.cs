﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CG_Project_4
{
    public partial class Form1 : Form
    {
        private bool down = false;
        private PointF x1;
        private PointF x2;
        private bool x1_full = false;
        private bool x2_full = false;

        private RectangleF rectangle_borders= new RectangleF(0,0,600,600);

        public Form1()
        {
            InitializeComponent();
        }

        //Draw squere//
        private void draw_rectangle_button_Click(object sender, EventArgs e)
        {
            Random r = new Random();
            //int n = r.Next(1,500);

            Graphics formGraphics = drawing_panel.CreateGraphics();
            //Clear panel
            formGraphics.Clear(Color.White);
            drawing_panel.Refresh();
            Pen blackPen = new Pen(Color.Black, 3);

           
            int width = Convert.ToInt32(squeresizex_textBox.Text);
            int height = Convert.ToInt32(squeresizey_textBox.Text); ;
            int x = r.Next(1, 600-width);
            int y = r.Next(1, 600-height);

            Rectangle rect = new Rectangle(x,y,width,height);
            this.rectangle_borders = new RectangleF((float)x, (float)y, (float)width, (float)height);
            formGraphics.DrawRectangle(blackPen, rect);
            formGraphics.Dispose();

            
        }

        //Clear panel//
        private void clear_button_Click(object sender, EventArgs e)
        {
            rectangle_borders = new RectangleF(0, 0, 600, 600);
            Graphics formGraphics = drawing_panel.CreateGraphics();
            //Clear panel
            formGraphics.Clear(Color.White);
            drawing_panel.Refresh();
            formGraphics.Dispose();
        }

        //Linag-Barsky Algorithm
        #region Liang_Barsky

        //Clipping Algorithm
        private bool Clip(float denom, float numer, ref float tE,  ref float tL)
        {
            if (denom == 0) //Paralel line
                { 
                    if ( numer > 0)
                        return false; // outside - discard
                    return true; //skip to next edge
                }

                float t = numer/denom;

                if (denom > 0) //PE
                { 
                    if (t > tL) //tE > tL - discard
                        return false;
                
                    if (t > tE)
                          tE = t;
                }

                else  //denom < 0 - PL
                {
                    if ( t < tE ) //tL < tE - discard
                        return false;

                    if ( t < tL )
                            tL = t;
                }

                return true;
        }

        private void LiangBarsky(PointF p1, PointF p2, RectangleF clip)
        {
            float dx = p2.X - p1.X;
            float dy = p2.Y - p1.Y;
            //tmin
            float tE = 0;
            //tmax
            float tL = 1;

            if (Clip(-dx, p1.X - clip.Right, ref tE, ref tL))
                  if (Clip(dx, clip.Left - p1.X, ref tE, ref tL))
                        if (Clip(-dy, p1.Y - clip.Bottom, ref tE, ref tL))
                            if (Clip(dy, clip.Top - p1.Y, ref tE, ref tL))
                            {
                                if (tL < 1)
                                {
                                    p2.X = p1.X + dx * tL;
                                    p2.Y = p1.Y + dy * tL;
                                }

                                if (tE > 0)
                                {
                                    p1.X += dx * tE;
                                    p1.Y += dy * tE;
                                }

                                Graphics g = drawing_panel.CreateGraphics();
                                Pen myPen = new Pen(Color.Black);
                                myPen.Width = 2;
                                g.DrawLine(myPen, p1, p2);
                            }
        }

        #endregion Liang_Barsky

        //Drawing line
        #region Line
        
        private void drawing_panel_MouseDown(object sender, MouseEventArgs e)
        {

            Brush aBrush = (Brush)Brushes.Black;
            Graphics g = drawing_panel.CreateGraphics();
            Pen myPen = new Pen(Color.Black);
            myPen.Width = 3;
            down = true;

            // if down true, assignt to position location of mouse cursor
            if (down)
            {

                if (x1_full != true)
                {
                    x1.X = e.Location.X;
                    x1.Y = e.Location.Y;
                    g.FillRectangle(aBrush, x1.X, x1.Y, 3, 3);
                    x1_full = true;
                }
                else if (x1_full == true && x2_full != true)
                {
                    x2.X = e.Location.X;
                    x2.Y = e.Location.Y;
                    g.FillRectangle(aBrush, x2.X, x2.Y, 3, 3);

                    //Draw line
                    //g.DrawLine(myPen,x1.X, x1.Y, x2.X, x2.Y);
                    LiangBarsky(x1,x2,rectangle_borders);
                    
                    x2_full = true;
                    x1_full = false;
                    x2_full = false;
                }
                else
                {
                    MessageBox.Show("Points full");
                }

            }


        }

        private void drawing_panel_MouseUp(object sender, MouseEventArgs e)
        {
            down = false;
        }

        #endregion Line


    }
}
