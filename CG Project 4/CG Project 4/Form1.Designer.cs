﻿namespace CG_Project_4
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.drawing_panel = new System.Windows.Forms.Panel();
            this.draw_rectangle_button = new System.Windows.Forms.Button();
            this.clear_button = new System.Windows.Forms.Button();
            this.squeresizex_textBox = new System.Windows.Forms.TextBox();
            this.squeresizey_textBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // drawing_panel
            // 
            this.drawing_panel.BackColor = System.Drawing.Color.White;
            this.drawing_panel.Location = new System.Drawing.Point(155, 12);
            this.drawing_panel.Name = "drawing_panel";
            this.drawing_panel.Size = new System.Drawing.Size(600, 600);
            this.drawing_panel.TabIndex = 0;
            this.drawing_panel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.drawing_panel_MouseDown);
            this.drawing_panel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.drawing_panel_MouseUp);
            // 
            // draw_rectangle_button
            // 
            this.draw_rectangle_button.BackColor = System.Drawing.Color.SeaShell;
            this.draw_rectangle_button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.draw_rectangle_button.Location = new System.Drawing.Point(12, 12);
            this.draw_rectangle_button.Name = "draw_rectangle_button";
            this.draw_rectangle_button.Size = new System.Drawing.Size(126, 24);
            this.draw_rectangle_button.TabIndex = 0;
            this.draw_rectangle_button.Text = "Draw squere";
            this.draw_rectangle_button.UseVisualStyleBackColor = false;
            this.draw_rectangle_button.Click += new System.EventHandler(this.draw_rectangle_button_Click);
            // 
            // clear_button
            // 
            this.clear_button.BackColor = System.Drawing.Color.SeaShell;
            this.clear_button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.clear_button.Location = new System.Drawing.Point(12, 42);
            this.clear_button.Name = "clear_button";
            this.clear_button.Size = new System.Drawing.Size(126, 23);
            this.clear_button.TabIndex = 0;
            this.clear_button.Text = "Clear";
            this.clear_button.UseVisualStyleBackColor = false;
            this.clear_button.Click += new System.EventHandler(this.clear_button_Click);
            // 
            // squeresizex_textBox
            // 
            this.squeresizex_textBox.Location = new System.Drawing.Point(12, 91);
            this.squeresizex_textBox.Name = "squeresizex_textBox";
            this.squeresizex_textBox.Size = new System.Drawing.Size(57, 20);
            this.squeresizex_textBox.TabIndex = 0;
            this.squeresizex_textBox.Text = "200";
            // 
            // squeresizey_textBox
            // 
            this.squeresizey_textBox.Location = new System.Drawing.Point(75, 91);
            this.squeresizey_textBox.Name = "squeresizey_textBox";
            this.squeresizey_textBox.Size = new System.Drawing.Size(63, 20);
            this.squeresizey_textBox.TabIndex = 0;
            this.squeresizey_textBox.Text = "200";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "width:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(72, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "height:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkOrange;
            this.ClientSize = new System.Drawing.Size(765, 620);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.squeresizey_textBox);
            this.Controls.Add(this.squeresizex_textBox);
            this.Controls.Add(this.clear_button);
            this.Controls.Add(this.draw_rectangle_button);
            this.Controls.Add(this.drawing_panel);
            this.Name = "Form1";
            this.Text = "CG Project 4";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel drawing_panel;
        private System.Windows.Forms.Button draw_rectangle_button;
        private System.Windows.Forms.Button clear_button;
        private System.Windows.Forms.TextBox squeresizex_textBox;
        private System.Windows.Forms.TextBox squeresizey_textBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

